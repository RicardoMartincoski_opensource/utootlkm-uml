[TOC]

# utootlkm-uml

------

## Description
**utootlkm-uml** stands for *Unit Tests for Out-Of-Tree Linux Kernel Modules,
User-Mode Linux variant*.

This project can be used as an infrastructure to run unit tests for any open
source Linux kernel module that is maintained in its own repository (out of the
main Linux git tree).

------

## License

SPDX-License-Identifier: GPL-2.0-only

    utootlkm-uml
    Copyright (C) 2023  Ricardo Martincoski  <ricardo.martincoski@gmail.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; version 2.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

------

There are some files in the tree copied from other projects.
All of such files were originally licensed using a license that allows them to
be redistributed following GPL-2.0.
They have the license stated in the header of the file. For instance:

    SPDX-License-Identifier: GPL-2.0-or-later
    copied from <Project name> <version>
    and then updated:
    - to do <something nice>
